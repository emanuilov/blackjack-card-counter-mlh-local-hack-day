var url = "https://hobrien.ddns.net/api/",
	user = {
		id: null,
		name: null
	};

function createUser(username) {
	$.ajax({
		url: url + "/create.php",
		type: "POST",
		data: {
			"type": "createUser",
			"username": username
		},
		success: function (data) {
			var result = JSON.parse(data);
			if (result.status == true) {
				return result.userID;
			}
		},
		error: function () {
			setTimeout(function () {
				createUser(username);
			}, 1000);
		}
	});
	return userID;
}

function createGame(gameName, deckCount, minBet) {
	$.ajax({
		url: url + "/create.php",
		type: "POST",
		data: {
			"type": "createGame",
			"gameName": gameName,
			"deckCount": deckCount,
			"minBet": minBet
		},
		success: function (data) {
			var result = JSON.parse(data);
			if (result.status == true) {
				$(".popup").trigger("click");
				$('table').append('<tr><td>' + result.gameName + '</td><td>Active</td><td>1/5</td><td><div class="btn btn-success" data-id="' + result.id + '">Join</div><div class="btn btn-secondary" data-id="1">Spectate</div></td></tr>');
			}
		},
		error: function () {
			setTimeout(function () {
				createUser(username);
			}, 1000);
		}
	});
	return userID;
}

function updateGame(userID) {
	var gameboard = $('.container.current-game .gameboard');
	$.ajax({
		url: url + "/read.php",
		type: "POST",
		data: {
			"type": "gameStatus",
			"playerID": user.id
		},
		success: function (data) {
			var result = JSON.parse(data);
			if (result.status == true) {
				var playerData = $('.header');
				playerData.find(".left span").html(result.player.money);
				playerData.find(".right p:last-of-type").html(result.player.status);
				if (result.player.status == "Your go") {
					var buttons = $('.player-cards .left, .player-cards .right');
					buttons.css("opacity", 1);
					buttons.css("pointer-events", "all");
				} else {
					var buttons = $('.player-cards .left, .player-cards .right');
					buttons.css("opacity", 0.5);
					buttons.css("pointer-events", "none");
				}

				var dealer = gameboard.find('.dealer-cards');
				dealer.empty();
				for (var i = 0; i < result.dealerCards.length; i++) {
					dedaler.append('<img src="img/cards/front/' + result.value + result.suit + '.png" alt="Card">');
				};

				var player_cards = gameboard.find('.player-cards');
				player_cards.empty();
				for (var i = 0; i < result.player.cards.length; i++) {
					player_cards.append('<img src="img/cards/front/' + result.player.cards[i].value + result.player.cards[i].suit + '.png" alt="Card">');
				}

				var otherCards = gameboard.find('.other-poeple-cards .player');
				for (var i = 0; i < otherCards.length; i++) {
					otherCards[i].empty();
					var cardsBox = result.cardsBox[otherCards.attr('id')];
					for (var i = 0; i < cardsBox.length; i++) {
						otherCards[i].append('<img src="img/cards/front/' + cardsBox[i].value + cardsBox[i].suit + '.png" alt="Card">');
					}
				}
			}
		},
		error: function () {
			setTimeout(function () {
				updateGame();
			}, 1000);
		}
	});
}
var counter = 0;
var cardNames = ["7H", "8H"];
var values = [7, 8]
var sum = 0;

function requestCard(requestType, playerID) {
	console.log("dsadsadsa");
	var el = $('.player-cards .center');
	el.append('<img src="img/cards/front/' + cardNames[counter] + '.png" alt="Card">');
	sum += values[counter];
	counter += 1;
	if (cardsCounter(values[counter]) > 0.5) {
		$('.text-panel strong').html("Stand");
	} else {
		$('.text-panel strong').html("Hit");
	}
	el.css("overflow", "auto");
	/*	$.ajax({
			url: url + "/read.php",
			type: "POST",
			data: {
				"type": "requestCard",
				"requestType": requestType,
				"playerID": playerID
			},
			success: function (data) {
				var result = JSON.parse(data);
				if (result.status == true) {
					var el = $('.player-cards .center');
					el.append('<img src="img/cards/front/' + result.value + result.suit + '.png" alt="Card">');
					el.css("overflow", "auto");
				}
			},
			error: function () {
				setTimeout(function () {
					requestCard(money, playerID);
				}, 1000);
			}
		});*/
}

function makeBet(money, playerID) {
	$.ajax({
		url: url + "/update.php",
		type: "POST",
		data: {
			"type": "bet",
			"money": money,
			"playerID": playerID
		},
		success: function (data) {
			var result = JSON.parse(data);
			if (result == true) {
				var el = $(".money");
				el.html(parseInt(el.html()) - money);
			}
		},
		error: function () {
			setTimeout(function () {
				makeBet(money, playerID);
			}, 1000);
		}
	});
}