function sumOfHand(cards) {
	var sum = 0;
	for (card in cards) {
		sum += card.value;
	}
	return sum;
}
var cardsCount = 0;

function cardsCounter(sumOfHand) {
	var bustChance = 0;
	// var sumOfHand = sumOfHand(result.player.cards);
	var decsCount = 1;
	cardsCount += 1; //cards drawn from all the decs
	var cardsRemaining = 52 * decsCount - cardsCount; //cards remaining in all decs
	var magicNumber = sumOfHand - 12;
	var value = 0;

	var cards = {
		1: 0,
		2: 0,
		3: 0,
		4: 0,
		5: 0,
		6: 0,
		7: 0,
		8: 0,
		9: 0
	};
	var cardsTen = 0;

	if (value == cards[value]) {
		cards[value]++;
	} else if (value == 10 || value == "K" || value == "Q" || value == "J") {
		cardsTen++;
	}

	function bustTen() {
		bustChance = decsCount / cardsRemaining * (16 - cardsTen / decsCount);
	}

	if (sumOfHand == 12) {
		bustTen();
	} else if (sumOfHand > 12) {
		bustTen();
		bustChance += 4 * magicNumber / cardsRemaining;
		var current = 9;
		var extra = 0;
		for (var j = 1; j < magicNumber; j++) {
			extra += cards[current];
			current--;
		}
		bustChance += extra / cardsRemaining;
	} else {
		bustChance = 0;
	}
	return bustChance;
}